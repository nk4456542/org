---
title: Languages and Frameworks
description: An overview of the Languages and Frameworks we use at Grey Software
category: Tools and Tech
position: 19
---

## Vue

![vue Preview](/tech-stack/vue-preview.png)

<cta-button link="https://vuejs.org" text="Learn More" > </cta-button>

## Nuxt

![Nuxt Preview](/tech-stack/nuxt-preview.png)

<cta-button link="https://nuxtjs.org" text="Learn More" > </cta-button>

## Quasar

![Quasar Preview](/tech-stack/quasar-preview.png)

<cta-button link="https://quasar.dev" text="Learn More" > </cta-button>
